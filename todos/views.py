from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.

def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list.html', {'todo_lists':todo_lists})

def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    return render(request, 'todos/todo_detail.html', {'todo_list':todo_list})

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todo_list_create.html', {'form':form})

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(request, 'todos/todo_list_update.html', {'form': form})

def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/todo_list_delete.html', {'todo_list':todo_list})


# def todo_item_create(request,id):
#     todo_list = get_object_or_404(TodoList, id=id)
#     if request.method == 'POST':
#         form = TodoItemForm(request.POST)
#         if form.is_valid():
#             todo_item = form.save()
#             todo_item.list = todo_list
#             todo_item.save()
#             return redirect('todo_list_detail', id = todo_item.list.id)
#     else:
#         form = TodoItemForm()
#     return render(request, 'todos/todo_item_create.html', {'form':form})

def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', id = todo_item.list.id)
    else:
        form = TodoItemForm()
    return render(request, 'todos/todo_item_create.html', {'form':form})


def todo_item_update(request, id):
    post = TodoItem.objects.get(id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=post)
        if form.isvalid():
            form.save()
            return redirect('todo_list_detail', id=post.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        'post_object':post,
        'form':form,
    }
    return render(request, 'todos/todo_item_update.html', context)
# def todo_item_update(request):
#     if request.method == 'POST':
#         form = TodoItemForm(request.POST)
#         if form.isvalid():
#             todo_item = form.save()
#             return redirect('todo_list_detail', id=todo_item.list.id)
#     else:
#         form = TodoItemForm()
#     return render(request, 'todos/todo_item_update.html', {'form':form})
