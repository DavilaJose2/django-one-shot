from django.contrib import admin
from todos.models import TodoList
from todos.models import TodoItem

# Register your models here.
@admin.register(TodoList)
class TodoListAdmin (admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(TodoItem)
class ToddoItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'task', 'due_date', 'is_completed', 'list')
